# Ring light (Arduino) #

You can read [a post about this project](https://medium.com/@lorenzowood/make-a-multi-coloured-led-ring-light-for-event-photography-b1c04ba8913a#.s7eq2wpud)

Simple software to control a ring light made from Adafruit Neopixels using this circuit:
![ring light circuit.png](https://bitbucket.org/repo/9k4Laz/images/4102416133-ring%20light%20circuit.png)

You need to install the Adafruit Neopixel library in your Arduino environment.

## How to use it ##

There are three pushbuttons in the circuit:

1. Button 0 is the mute button: press it to toggle all the pixels on and off.

2. Button 1 cycles through available colours for half the ring

3. Button 2 cycles through available colours for the other half of the ring

If both halves of the ring are set to the same colour then all pixels will be the same colour. Otherwise the colour will vary smoothly across the ring between the two colours. You get an effect like this:

![DSC_0732.jpg](https://bitbucket.org/repo/9k4Laz/images/1372065212-DSC_0732.jpg)

## Ways to extend it ##

If you build this circuit:

* You could add a hard power switch rather than relying on the "soft" mute button; this would save battery.

* You could put the controller and battery in a box with a tripod screw for easy attachment to the camera. Or, you could use a hot-shoe-compatible mount.

* You could modify the circuit to be triggered by a hot shoe and therefore operate as a flash.

* You could add more colours to the software; add more colour points or other patterns; add a brightness control (either with up/down buttons or with an analogue knob).