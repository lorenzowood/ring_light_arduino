#include <Adafruit_NeoPixel.h>
#include <math.h>

#define PIN 6
#define NUMBER_OF_PIXELS 60

#define MUTE_PIN 0
#define COLOR1_PIN 1
#define COLOR2_PIN 2

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMBER_OF_PIXELS, PIN, NEO_GRB + NEO_KHZ800);

/*** HELPERS ***/

#define PI 3.1415926535

long getR(long c) { return c>>16; }
long getG(long c) { return (c>>8)&0xff; }
long getB(long c) { return c&0xff; }

long makeColor(long r, long g, long b) {  return r<<16 | g<<8 | b; }

long mixColor(long colorA, long colorB, float proportion) {
  long p = proportion * 1000;
  return makeColor( map(p, 0, 1000, getR(colorA), getR(colorB)), map(p, 0, 1000, getG(colorA), getG(colorB)), map(p, 0, 1000, getB(colorA), getB(colorB)));
}

long addColor(long colorA, long colorB) {
  return makeColor( min(255, getR(colorA) + getR(colorB)), min(255, getG(colorA) + getG(colorB)), min(255, getB(colorA) + getB(colorB)));
}

/*** END OF HELPERS ***/

void setup() {
  strip.begin();
  strip.setBrightness(128);
  strip.show();
  Serial.begin(9600);
  
  pinMode(MUTE_PIN, INPUT_PULLUP);	// INPUT_PULLUP switches on internal pull-up resistors in the range 20K–50K
  pinMode(COLOR1_PIN, INPUT_PULLUP);
  pinMode(COLOR2_PIN, INPUT_PULLUP);
  setupRender();

}

void loop() {
  render();
}

/*** DEFINE AVAILABLE COLORS ***/
// Add more by all means. Adding too many will make it boring to cycle through them one at a time, so consider improving the control mechanism

#define NUMBER_OF_COLORS 6
long colors[NUMBER_OF_COLORS];

void setupRender() {
  colors[0] = makeColor(255,0,0);
  colors[1] = makeColor(255,255,0);
  colors[2] = makeColor(0,255,0);
  colors[3] = makeColor(0,0,255);
  colors[4] = makeColor(0,0,128);
  colors[5] = makeColor(255,0,255);
}

/*** END OF DEFINE AVAILABLE COLORS ***/

bool mute = false;
int colorIndex1 = 0;
int colorIndex2 = 1;

void render() {
  int i;
  
  if (digitalRead(COLOR1_PIN) == 0) {
    colorIndex1 = (colorIndex1 + 1) % NUMBER_OF_COLORS;
    delay(500);	// delay for crude debouncing
  }
  
  if (digitalRead(COLOR2_PIN) == 0) {
    colorIndex2 = (colorIndex2 + 1) % NUMBER_OF_COLORS;
    delay(500);
  }  
  if (digitalRead(MUTE_PIN) == 0) {
    mute = !mute;
    delay(500);
  }

  long pixelColor;
  
  for (i=0; i< NUMBER_OF_PIXELS; i++) {
    if (mute)
      pixelColor = makeColor(0, 0, 0);  // mute => all pixels off
    else
      pixelColor = mixColor(colors[colorIndex1], colors[colorIndex2], 0.5 + 0.5 * sin( 2.0 * PI * i / NUMBER_OF_PIXELS));
   
      strip.setPixelColor(i, pixelColor);
  }
  strip.show();
}


